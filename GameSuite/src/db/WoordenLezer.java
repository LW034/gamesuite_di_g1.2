package db;

import java.io.File;
import java.util.Scanner;

import domain.WoordenLijst;

public class WoordenLezer {

	
	public static WoordenLijst lees(String bestand){
		WoordenLijst result = new WoordenLijst();
		File file = new File(bestand);
		try{
			Scanner scanner = new Scanner(file);
			while(scanner.hasNextLine()){
				Scanner scannerlijn = new Scanner(scanner.nextLine());
				String woord = scannerlijn.next();
				if(!result.bevat(woord)){
				result.voegToe(woord);
				}
				scannerlijn.close();
			}
			scanner.close();
		}
		catch(Exception e){
			System.out.println(e.getMessage());
		}
		return result;
	}
}
