package ui;

import javax.swing.JOptionPane;

import domain.*;

public class PictionaryUi {
	

private Speler speler;

public PictionaryUi(Speler speler){
	this.speler = speler;
	this.showMenu();
	
}

public void showMenu(){
	String naam = JOptionPane.showInputDialog("Naam tekening");
	Tekening result = new Tekening(naam);
	GameHoofdScherm scherm = new GameHoofdScherm(naam,result);
	String input = JOptionPane.showInputDialog("Maak een Keuze\n1 Maak een Vorm\n2 Toon Vormen\n3 Toon Tekening\n0 Stoppen");
	while(!input.equals("0")){
		if(input.equals("1")){
			Vorm a = this.maakVorm();
			a.setZichtbaar(true);
			result.voegToe(a);
		}
		if(input.equals("2")){
			
			JOptionPane.showMessageDialog(null,"Tekening met naam "+result.getNaam()+" bestaat uit "+result.getAantalVormen()+" Vormen"+"\n"+result.toString());
		}
		if(input.equals("3")){
			scherm.setVisible(true);
			scherm.teken();
		}
		input = JOptionPane.showInputDialog("Maak een Keuze\n1 Maak een Vorm\n2 Toon Vormen\n3 Toon Tekeningn\n0 Stoppen");
		scherm.setVisible(false);
	}
}
public Vorm maakVorm(){
	Object[] shapes = {"Cirkel", "Rechthoek","Driehoek", "LijnStuk"};
	Object keuze = JOptionPane.showInputDialog(null,"Wat wil u tekenen?","input",JOptionPane.INFORMATION_MESSAGE,null,shapes,null);
	Vorm result = null;
	String vorm = (String)keuze;
	switch(vorm){
	case "Cirkel":
		result = this.maakCirkel();
		break;
	case "Rechthoek":
		result = this.maakRechthoek();
		break;
	case "Driehoek":
		result = this.maakDriehoek();
		break;
	case "LijnStuk":
		result = this.maakLijnStuk();
		break;
	}
	return result;
}

	public Cirkel maakCirkel(){
		JOptionPane.showMessageDialog(null, "Een Cirkel bestaat uit een MiddelPunt en een Radius");
		Cirkel a = null;
		Punt middelPunt = null;
		int radius = 0;
		boolean result = false;
		while(!result){
		try{
		middelPunt = this.maakPunt("van het Middelpunt");
		result = true;
		}
		catch (Exception e){
			JOptionPane.showMessageDialog(null, e.getMessage());
			result = false;
		}
		}
		result = false;
		while(!result){
		try{
		radius = Integer.parseInt(JOptionPane.showInputDialog("Radius?"));
		a = new Cirkel(middelPunt,radius);
		result = true;
		}
		catch(Exception e){
			JOptionPane.showMessageDialog(null, e.getMessage());
			result = false;
		}
		}
		JOptionPane.showMessageDialog(null, a.toString());
		return a;
	}
	
	public Rechthoek maakRechthoek(){
		Punt linkerbovenhoek = new Punt(10,10);
		int breedte = 20;
		int hoogte = 40;
		Rechthoek b = new Rechthoek(linkerbovenhoek, breedte, hoogte);
		boolean result = false;
		JOptionPane.showMessageDialog(null, "Een Rechthoek bestaat uit een Linkerbovenhoek, een Breedte en een Hoogte");
		while(!result){
			try{
				breedte = Integer.parseInt(JOptionPane.showInputDialog("Breedte van de Rechthoek?"));
				b.setBreedte(breedte);
				result = true;
			}
			catch(Exception e){
			JOptionPane.showMessageDialog(null, e.getMessage());
			}
		}
		result = false;
		while(!result){
			try{
				hoogte = Integer.parseInt(JOptionPane.showInputDialog("Hoogte van de Rechthoek"));
				b.setHoogte(hoogte);
				result = true;
			}
			catch(Exception e){
				JOptionPane.showMessageDialog(null, e.getMessage());
			}
		}
		linkerbovenhoek = this.maakPunt("van de Linkerbovenhoek");
		b = new Rechthoek(linkerbovenhoek, breedte, hoogte);
		JOptionPane.showMessageDialog(null, b.toString());
		return b;
	}
	
	public Driehoek maakDriehoek(){
		JOptionPane.showMessageDialog(null, "Een Driehoek bestaat uit 3 hoekPunten");
		Punt hoekPunt1 = this.maakPunt("van HoekPunt 1");
		Punt hoekPunt2 = this.maakPunt("van HoekPunt 2");
		Punt hoekPunt3 = this.maakPunt("van HoekPunt 3");
		Driehoek c = new Driehoek(hoekPunt1, hoekPunt2, hoekPunt3);
		JOptionPane.showMessageDialog(null, c.toString());
		return c;
	}
	public LijnStuk maakLijnStuk(){
		JOptionPane.showMessageDialog(null, "Een Lijnstuk bestaat uit een Start en Eindpunt");
			Punt startPunt = this.maakPunt("van het Startpunt");
			Punt eindPunt = this.maakPunt("van het Eindpunt");
			
		LijnStuk d = new LijnStuk(startPunt, eindPunt);
		JOptionPane.showMessageDialog(null, d.toString());
		return d;
	}
	
	public Punt maakPunt(String in_te_geven_tekst){
	Punt punt = new Punt();
	boolean a = false;
	while(!a){
	try{
		punt.setX(Integer.parseInt(JOptionPane.showInputDialog("X-Coördinaat "+in_te_geven_tekst)));
		a = true;
		}
		catch(Exception e){
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
	}
	a = false;
	while(!a){
	try{
		punt.setY(Integer.parseInt(JOptionPane.showInputDialog("Y-Coördinaat "+in_te_geven_tekst)));
		a = true;
		}
	catch(Exception e){
		JOptionPane.showMessageDialog(null, e.getMessage());
		}
	}
	JOptionPane.showMessageDialog(null, "U heeft een correct Punt aangemaakt: "+punt.toString());
	return punt;
	}
}

