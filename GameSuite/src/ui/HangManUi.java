package ui;

import javax.swing.JOptionPane;

import db.WoordenLezer;
import domain.HangMan;
import domain.Speler;

public class HangManUi {
	
	public static void main(String[] args){
	String naam = JOptionPane.showInputDialog("Welkom! \nHoe heet je?");
	Speler speler = new Speler(naam);
	HangMan result = new HangMan(speler, WoordenLezer.lees("woorden.txt"));
	HangmanPaneel paneel = new HangmanPaneel(result);
	HangManHoofdScherm scherm = new HangManHoofdScherm(result, paneel);
	scherm.start();
	}
}
