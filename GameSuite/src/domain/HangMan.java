package domain;

public class HangMan 
{
	
	private boolean gewonnen = false;
	private boolean gameover = false;
	
	private Speler speler;
	private TekeningHangMan hangman;
	
	private HintWoord hint;
	private WoordenLijst woorden;
	
	public HangMan(Speler speler, WoordenLijst woorden)
	{
		this.setSpeler(speler);
		this.setWoorden(woorden);
		this.setHint(new HintWoord(this.getWoorden().getRandomWoord()));
		
		this.setHangman(new TekeningHangMan());
	}
	
	public Speler getSpeler()
	{
		return this.speler;
	}
	
	public TekeningHangMan getTekening()
	{
		return this.hangman;
	}
	
	public String getHint()
	{
		return this.hint.toString();
	}
	
	public String geefHint()
	{
		return this.getHint().toString();
	}
	
	public boolean isGewonnen()
	{
		return this.gewonnen;
	}
	
	public boolean isGameOver()
	{
		return this.gameover;
	}
	
	public void raad(char letter)
	{
		if (!this.hint.raad(letter)) this.getTekening().zetVolgendeZichtbaar();
		
		if (this.hint.isGeraden()) this.setGewonnen(true);
		if (this.getTekening().getAantalOnzichtbaar() == 0) this.setGameover(true);
	}
	
	private void setSpeler(Speler speler) throws DomainException {
		if ( speler == null ) {
			throw new DomainException("Player not valid");
		}
		this.speler = speler;
	}
	
	public WoordenLijst getWoorden()
	{
		return this.woorden;
	}
	
	private void setWoorden(WoordenLijst woorden) throws DomainException
	{
		if ( woorden == null || woorden.getAantalWoorden() == 0 ) throw new DomainException ("Wordlist not valid");
		this.woorden = woorden;
	}
	
	public void init()
	{
		setHint(new HintWoord(this.getWoorden().getRandomWoord()));
		setGameover(false);
		setGewonnen(false);
		this.getTekening().reset();
	}
	
	private void setHangman(TekeningHangMan hangman)
	{
		this.hangman = hangman;
	}
	
	private void setHint(HintWoord hint)
	{
		this.hint = hint;
	}
	
	private void setGewonnen(boolean gewonnen)
	{
		this.gewonnen = gewonnen;
	}
	
	private void setGameover(boolean gameover)
	{
		this.gameover = gameover;
	}

}
