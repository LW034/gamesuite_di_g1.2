package domain;

public class Omhullende {

	private int breedte,hoogte;
	private Punt linkerBovenhoek;
	
	public Omhullende(Punt linkerbovenhoek,int breedte,int hoogte){
		this.setBreedte(breedte);
		this.setHoogte(hoogte);
		this.setLinkerbovenhoek(linkerbovenhoek);
	}
	
	public int getBreedte() {
		return breedte;
	}
	public void setBreedte(int breedte) throws DomainException {
		if(breedte < 0){
			throw new DomainException("Ongeldige Breedte");
		}
		this.breedte = breedte;
	}
	public int getHoogte() {
		return hoogte;
	}
	public void setHoogte(int hoogte) throws DomainException {
		if(hoogte < 0){
			throw new DomainException("Ongeldige Hoogte");
		}
		this.hoogte = hoogte;
	}
	public Punt getLinkerBovenhoek() {
		return linkerBovenhoek;
	}
	public void setLinkerbovenhoek(Punt linkerbovenhoek) throws DomainException {
		if(linkerbovenhoek == null){
			throw new DomainException("Ongeldig Punt");
		}
		this.linkerBovenhoek = linkerbovenhoek;
	}
	@Override
	public boolean equals(Object o){
		if(o instanceof Omhullende){
			Omhullende a = (Omhullende) o;
			if(this.getLinkerBovenhoek().equals(a.getLinkerBovenhoek())
					&& this.getBreedte() == a.getBreedte()
					&& this.getHoogte() == a.getHoogte()){
				return true;
			}
		}
		return false;
	}
	
	public int getMinimumX(){
		return linkerBovenhoek.getX();
	}
	
	public int getMaximumX(){
		return linkerBovenhoek.getX()+breedte;
	}
	
	public int getMinimumY(){
		return linkerBovenhoek.getY();
	}
	
	public int getMaximumY(){
		return linkerBovenhoek.getY()+hoogte;
	}
	
	public String toString(){
		return "Omhullende: " + linkerBovenhoek.toString() + " - " + breedte + " - " + hoogte;
	}
	

}
