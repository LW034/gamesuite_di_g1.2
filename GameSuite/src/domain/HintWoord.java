package domain;

import java.util.ArrayList;

public class HintWoord {

	private String woord;
	private boolean isGeraden;
	private ArrayList<HintLetter> letter;
	
	public HintWoord(String woord){
		if(woord == null || woord.trim().equals("")){
			throw new DomainException("Ongeldig woord");
		}
		this.woord = woord.toString();
		letter = new ArrayList<HintLetter>();
		for(char a : woord.toCharArray()){
			letter.add(new HintLetter(a));
		}
	}
	
	public boolean raad(char letter){
		letter = Character.toLowerCase(letter);
		if(woord.indexOf(letter) != -1){
		for(int i = 0; i < this.letter.size();i++){
			if(woord.charAt(i) == letter){
				if(this.letter.get(i).isGeraden()){
					return false;
				}
				this.letter.get(i).raad(letter);
			}
		}
		return true;
		}
		else {
			return false;
		}
	}
	
	
	public boolean isGeraden(){
		for(HintLetter a : this.letter){
			if(!a.isGeraden()){
				return false;
			}
		}
		return true;
	}
	public String toString(){
		String result = "";
		for(int i = 0; i < letter.size();i++){
			result = result + letter.get(i).toChar() + " ";
		}
		result = result.substring(0, result.length()-1);
		return result;
		
	}
	public String getWoord(){
		return woord;
	}
	
}
