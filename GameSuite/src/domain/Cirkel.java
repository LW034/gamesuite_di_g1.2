package domain;

import java.awt.Graphics;

public class Cirkel extends Vorm implements Drawable {
	private Punt middelpunt;
	private int radius;
	
	public Cirkel(Punt middelpunt, int radius) {
		this.setMiddelpunt(middelpunt);
		this.setRadius(radius);
	}

	public Punt getMiddelpunt() {
		return this.middelpunt;
	}
	
	@Override
	public boolean equals(Object o) {
		if(o instanceof Cirkel){
			Cirkel c = (Cirkel)o;
			if(this.getMiddelpunt().equals(c.getMiddelpunt())&& this.getRadius() == c.getRadius()){
				return true;
				}
			}
		return false;
	}
	public String toString(){
		return super.toString()+" Middelpunt: " + middelpunt.toString() + ", Radius: " + radius;  
	}

	public void setMiddelpunt(Punt middelpunt) {
		if(middelpunt==null){
			throw new DomainException("Punt mag niet null zijn.");
		}
		this.middelpunt = middelpunt;
	}

	public int getRadius() {
		return radius;
	}

	private void setRadius(int radius) {
		if(radius<=0){
			throw new DomainException("Straal moet groter dan nul zijn.");
		}
		this.radius = radius;
	}

	@Override
	public Omhullende getOmhullende() {
		
		Punt linkerbovenhoek = new Punt(this.getMiddelpunt().getX()-radius,this.getMiddelpunt().getY()-radius);
		Omhullende result = new Omhullende(linkerbovenhoek, radius*2, radius*2);
		return result;
	}
	
	@Override 
	public void teken(Graphics graphics){
		if(super.isZichtbaar()){
		graphics.drawOval(this.getOmhullende().getMinimumX(), this
				.getOmhullende().getMinimumY(), this.getOmhullende()
				.getBreedte(), this.getOmhullende().getHoogte());
		}
	}
}
