package domain;

public class HintLetter {

	private char letter;
	private boolean isGeraden = false;
	public HintLetter(char letter){
		this.letter = letter;
	}
	public boolean raad(char letter){
		if(isGeraden == true && Character.toUpperCase(letter) == Character.toUpperCase(this.letter)){
			return false;
		}
		if(Character.toUpperCase(letter) == Character.toUpperCase(this.letter)){
			isGeraden = true;
			return true;
		}
		return false;
	}
	
	public boolean isGeraden(){
		return isGeraden;
	}
	public char toChar(){
		if(isGeraden()){
			return letter;
		}
		else {
			return '_';
		}
	}
	public char getLetter(){
		return letter;
	}
	@Override
	public boolean equals(Object o){
		if(o instanceof HintLetter){
			HintLetter a = (HintLetter) o;
			if(this.letter == a.letter || Character.toLowerCase(letter) == Character.toLowerCase(a.letter)
					|| Character.toUpperCase(letter) == Character.toUpperCase(a.letter) ){
				return true;
			}
		}
		return false;
	}
}
