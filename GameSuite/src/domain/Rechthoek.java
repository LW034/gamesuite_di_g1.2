package domain;

import java.awt.Graphics;

public class Rechthoek extends Vorm implements Drawable {

	private int breedte,hoogte;
	private Punt linkerbovenhoek;
	
	public Rechthoek(Punt linkerbovenhoek,int breedte,int hoogte){
		this.setHoogte(hoogte);
		this.setBreedte(breedte);
		this.setLinkerbovenhoek(linkerbovenhoek);
	}

	public int getBreedte() {
		return breedte;
	}

	public void setBreedte(int breedte) throws DomainException {
		if(breedte <= 0){
			throw new DomainException("Ongeldige Breedte");
		}
		this.breedte = breedte;
	}

	public int getHoogte() {
		return hoogte;
	}

	public void setHoogte(int hoogte) throws DomainException {
		if(hoogte <= 0){
			throw new DomainException("Ongeldige Hoogte");
		}
		this.hoogte = hoogte;
	}

	public Punt getLinkerBovenhoek() {
		return linkerbovenhoek;
	}

	public void setLinkerbovenhoek(Punt linkerbovenhoek) throws DomainException {
		if(linkerbovenhoek == null){
			throw new DomainException("Ongeldig Punt");
		}
		this.linkerbovenhoek = linkerbovenhoek;
	}
	
	@Override
	public boolean equals(Object o){
		if(o instanceof Rechthoek){
			Rechthoek a = (Rechthoek) o;
			if(super.equals(a)&&this.getLinkerBovenhoek().equals(a.getLinkerBovenhoek())
					&& this.getBreedte() == a.getBreedte()
					&& this.getHoogte() == a.getHoogte()){
				return true;
			}
		}
		return false;
	}
	@Override
	public String toString(){
		return super.toString()+" Breedte : "+getBreedte()+" - Hoogte : "+getHoogte()+" - LinkerbovenHoek : "+getLinkerBovenhoek();
	}

	@Override
	public Omhullende getOmhullende() {
		Omhullende result = new Omhullende(linkerbovenhoek, breedte, hoogte);
		return result;
	}
	
	@Override 
	public void teken(Graphics graphics){
		if(super.isZichtbaar()){
		graphics.drawRect(this.getLinkerBovenhoek().getX(), this
				.getLinkerBovenhoek().getY(), this.getBreedte(), this
				.getHoogte());
		}
	}
	
}
