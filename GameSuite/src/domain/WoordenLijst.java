package domain;

import java.util.ArrayList;

public class WoordenLijst {
private ArrayList<String> woorden;

	public WoordenLijst(){
		woorden = new ArrayList<String>();
	}
	
	public int getAantalWoorden(){
		return woorden.size();
	}
	public void voegToe(String woord){
		if(woord == null || woord.equals("") || woorden.contains(woord)){
			throw new DomainException("Ongeldig Woord");
		}
		woorden.add(woord);
	}
	public boolean bevat(String woord){
		return woorden.contains(woord);
	}
	public String toString(){
		String result = "";
		for (String woord : woorden){
			result = result.concat(woord)+"\n";
		}
		return result;
	}
	public String getRandomWoord(){
		int index = (int) Math.round(Math.random() * 10);
		while(index >= woorden.size()){
			index = (int) Math.round(Math.random() * 10);
		}
		return this.woorden.get(index);
	}

}
