package domain;

import java.awt.*;


public abstract class Vorm implements Drawable {
	
	private boolean zichtbaar = false;
	public void teken(Graphics graphics){
		if(this.isZichtbaar()){
			this.teken(graphics);
		}
	}

	@Override
	public boolean equals(Object o){
		if(o instanceof Vorm){
			Vorm a = (Vorm) o;
			if(this.getOmhullende().equals(a.getOmhullende())){
				return true;
			}
			
		}
		return false;
	}
	
	public boolean isZichtbaar() {
		return zichtbaar;
	}
	public void setZichtbaar(boolean zichtbaar){
		this.zichtbaar = zichtbaar;
	}
	public abstract Omhullende getOmhullende();
	
	
	@Override
	public String toString(){
		//return this.getOmhullende().toString();
		return "";
	}
}

