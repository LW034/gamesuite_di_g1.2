package domain;

import java.awt.Graphics;

public class Driehoek extends Vorm implements Drawable {

	private Punt hoekPunt1, hoekPunt2,hoekPunt3;
	
	public Driehoek(Punt hoekPunt1,Punt hoekPunt2, Punt hoekPunt3){
		this.setHoekPunten(hoekPunt1, hoekPunt2, hoekPunt3);
		
	}
	
	public void setHoekPunten(Punt hoekPunt1,Punt hoekPunt2,Punt hoekPunt3) throws DomainException {
		if(hoekPunt1 == null || hoekPunt2 == null || hoekPunt3 == null){
			throw new DomainException("Ongeldig HoekPunt");
		}
		this.hoekPunt1 = hoekPunt1;
		this.hoekPunt2 = hoekPunt2;
		this.hoekPunt3 = hoekPunt3;
	}
	public Punt getHoekPunt1(){
		return this.hoekPunt1;
	}
	public Punt getHoekPunt2(){	
		return this.hoekPunt2;
	}
	public Punt getHoekPunt3(){	
		return this.hoekPunt3;
	}
	
	@Override
	public boolean equals(Object o){
		if(o instanceof Driehoek){
			Driehoek a = (Driehoek) o;
			if(super.equals(a)&&this.getHoekPunt1().equals(a.getHoekPunt1())
					&& this.getHoekPunt2().equals(a.getHoekPunt2())
					&& this.getHoekPunt3().equals(a.getHoekPunt3())){
				return true;
			}
		}
		return false;
	}
	@Override
	public String toString(){
		return super.toString()+" HoekPunt1 : "+this.getHoekPunt1()+" , HoekPunt2 : "+this.getHoekPunt2()+" , HoekPunt3 : "+this.getHoekPunt3();
	}

	@Override
	public Omhullende getOmhullende() {
		int minX = Math.min(Math.min(this.getHoekPunt1().getX(),this.getHoekPunt2().getX()),this.getHoekPunt3().getX());
		int maxX = Math.max(Math.max(this.getHoekPunt1().getX(),this.getHoekPunt2().getX()),this.getHoekPunt3().getX());
		int minY = Math.min(Math.min(this.getHoekPunt1().getY(),this.getHoekPunt2().getY()),this.getHoekPunt3().getY());
		int maxY = Math.max(Math.max(this.getHoekPunt1().getY(),this.getHoekPunt2().getY()),this.getHoekPunt3().getY());
		Punt linkerbovenhoek = new Punt(minX, maxY);
		int breedte = maxX - minX;
		int hoogte = maxY - minY;
		Omhullende result = new Omhullende(linkerbovenhoek, breedte, hoogte);
		return result;
	}
	@Override 
	public void teken(Graphics graphics){
		if(super.isZichtbaar()){
		int[] xPoints = { this.getHoekPunt1().getX(), this.getHoekPunt2().getX(),
				this.getHoekPunt3().getX() };
		int[] yPoints = { this.getHoekPunt1().getY(), this.getHoekPunt2().getY(),
				this.getHoekPunt3().getY() };
		graphics.drawPolygon(xPoints, yPoints, 3);
		}
	}
}
