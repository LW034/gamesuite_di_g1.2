package domain;

import java.awt.Graphics;

public class LijnStuk extends Vorm implements Drawable {

	private Punt startPunt, eindPunt;
	
	public LijnStuk(Punt start,Punt eind) throws DomainException {
		this.setStartEnEindPunt(start, eind);
	}
	public void setStartEnEindPunt(Punt start,Punt eind) throws DomainException {
		if(start == null || eind == null){
			throw new DomainException("Ongeldig StartPunt of EindPunt");
		}
		this.startPunt = start;
		this.eindPunt = eind;
	}
	public Punt getStartPunt(){
		return this.startPunt;
	}
	public Punt getEindPunt(){
		return this.eindPunt;
	}
	@Override
	public boolean equals(Object o){
		if (o instanceof LijnStuk){
			LijnStuk a =  (LijnStuk) o;
			if(super.equals(a) && this.getStartPunt().equals(a.getStartPunt())
					&& this.getEindPunt().equals(a.getEindPunt())){
				return true;
			}
		}
		return false;
	}
	@Override
	public String toString(){
		return super.toString()+" StartPunt : "+this.getStartPunt()+" , EindPunt : "+this.getEindPunt();
	}
	@Override
	public Omhullende getOmhullende() {
		Punt linkerbovenhoek;
		int breedte;
		int hoogte;
		if(startPunt.getY() > eindPunt.getY()){
			linkerbovenhoek = new Punt(startPunt.getX(),startPunt.getY());
		}
		else{
			linkerbovenhoek = new Punt(startPunt.getX(), startPunt.getY());
		}
		breedte = eindPunt.getX() - startPunt.getX();
		hoogte = eindPunt.getY() - startPunt.getY();
		Omhullende result = new Omhullende(linkerbovenhoek,breedte,hoogte);
		return result;
	}
	@Override 
	public void teken(Graphics graphics){
		if(super.isZichtbaar()){
		graphics.drawLine(this.getStartPunt().getX(), this
				.getStartPunt().getY(), this.getEindPunt().getX(), this
				.getEindPunt().getY());
		}
	}
}
