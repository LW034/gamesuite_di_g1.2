
package domain;

public class Punt {
	
	private int xCoordinaat,yCoordinaat;
	
	public Punt(int xCoordinaat,int yCoordinaat) throws DomainException {
		this.setX(xCoordinaat);
		this.setY(yCoordinaat);
	}
	public Punt(){
		
	}
	
	public void setX(int xCoordinaat) throws DomainException {
		if(xCoordinaat < 0){
			throw new DomainException("Ongeldige X-Waarde");
		}
		this.xCoordinaat = xCoordinaat;
	}
	
	public void setY(int yCoordinaat)throws DomainException {
		if(yCoordinaat < 0){
			throw new DomainException("Ongeldige Y-Waarde");
		}
		this.yCoordinaat = yCoordinaat;
	}
	public int getX(){
		return xCoordinaat;
	}
	public int getY(){
		return yCoordinaat;
	}
	@Override
	public boolean equals(Object o){
		if (o instanceof Punt){
			Punt a = (Punt) o;
			if(this.getX() == a.getX() && this.getY() == a.getY()){
				return true;
			}
		}
		return false;
	}
	@Override
	public String toString(){
		return "("+this.getX()+ ", "+this.getY()+")";
	}
}
