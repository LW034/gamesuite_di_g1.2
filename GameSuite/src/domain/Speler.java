package domain;

public class Speler {
	private String naam;
	private int score =0;
	
	public Speler(String naam) {
		this.setNaam(naam);		
	}

	public String getNaam() {
		return naam;
	}

	public int getScore() {
		return score;
	}
	
	
	public void setNaam(String naam) {
		if(naam == null || naam.isEmpty()){
			throw new DomainException("naam mag niet leeg of null zijn");
		}
		this.naam = naam;
	}
	
	public void addToScore(int score){
		this.score = this.score + score;
		if(this.score<0){
			throw new DomainException("De resulterende score mag niet kleiner dan 0 zijn.");
		}
		
	} 
	@Override
	public boolean equals(Object o) {
		boolean result = false;
		if(o != null){
		if(o instanceof Speler){
			Speler s = (Speler)o;
			if(this.getNaam() == s.getNaam() && this.getScore() == s.getScore()){
				result = true;
				}
			}
		}
		return result;
	}
	
	
	public String toString(){
		return "Naam: " + naam + ", score: " + score;
	}

}
