package domain;

import java.util.ArrayList;


public class TekeningHangMan extends Tekening {
	
	private ArrayList<Vorm> hangmanvormen;
	private int aantalVormen, aantalonzichtbaar;
	
	public TekeningHangMan() {
		super("Hangman");
		hangmanvormen = new ArrayList<Vorm>();
		this.maakHangMan();
	}
	

	private void maakHangMan() {
		Vorm blokje = new Rechthoek(new Punt(10, 350), 300, 40);// altijd zichtbaar
		Vorm staaf = new LijnStuk(new Punt(160, 350), new Punt(160, 50));// altijd zichtbaar
		Vorm hangbar = new LijnStuk(new Punt(160, 50), new Punt(280, 50));// altijd zichtbaar
		Vorm koord = new LijnStuk(new Punt(280, 50), new Punt(280, 100));// altijd zichtbaar
		Vorm hoofd = new Cirkel(new Punt(280, 125), 25);// zichtbaar na 1 fout
		Vorm oogl = new Cirkel(new Punt(270, 118), 2);// zichtbaar na 2 fouten
		Vorm oogr = new Cirkel(new Punt(290, 118), 2);//
		Vorm neus = new Cirkel(new Punt(280, 128), 2);
		Vorm mond = new LijnStuk(new Punt(270, 138), new Punt(290, 138));
		Vorm lijf = new LijnStuk(new Punt(280, 150), new Punt(280, 250));
		Vorm beenl = new LijnStuk(new Punt(280, 250), new Punt(240, 310));
		Vorm beenr = new LijnStuk(new Punt(280, 250), new Punt(320, 310));
		Vorm voetl = new Cirkel(new Punt(240, 310), 5);
		Vorm voetr = new Cirkel(new Punt(320, 310), 5);
		Vorm arml = new LijnStuk(new Punt(280, 200), new Punt(230, 170));
		Vorm armr = new LijnStuk(new Punt(280, 200), new Punt(330, 170));
		Vorm handl = new Cirkel(new Punt(230, 170), 5);
		Vorm handr = new Cirkel(new Punt(330, 170), 5);
		
		hangmanvormen.add(blokje);
		hangmanvormen.add(staaf);
		hangmanvormen.add(hangbar);
		hangmanvormen.add(koord);
		hangmanvormen.add(hoofd);
		hangmanvormen.add(oogl);
		hangmanvormen.add(oogr);
		hangmanvormen.add(neus);
		hangmanvormen.add(mond);
		hangmanvormen.add(lijf);
		hangmanvormen.add(beenl);
		hangmanvormen.add(beenr);
		hangmanvormen.add(voetl);
		hangmanvormen.add(voetr);
		hangmanvormen.add(arml);
		hangmanvormen.add(armr);
		hangmanvormen.add(handl);
		hangmanvormen.add(handr);
		
		hangmanvormen.get(0).setZichtbaar(true);
		hangmanvormen.get(1).setZichtbaar(true);
		hangmanvormen.get(2).setZichtbaar(true);
		hangmanvormen.get(3).setZichtbaar(true);
		
		for (Vorm v : hangmanvormen) {
			super.voegToe(v);
		}
		
		this.aantalVormen = hangmanvormen.size();
		this.aantalonzichtbaar = aantalVormen-4;
	}
	
	public int getAantalOnzichtbaar() {
		return aantalonzichtbaar;
	}
	
	@Override
	public void voegToe(Vorm vorm){
		if(vorm.isZichtbaar()){
		hangmanvormen.add(vorm);
		}
	}
	
	@Override
	public void verwijder(Vorm vorm){
		hangmanvormen.remove(vorm);
	}
	
	public void reset() {
		for(int i = 4; i < hangmanvormen.size(); i++) {
			hangmanvormen.get(i).setZichtbaar(false);
		}
		this.aantalonzichtbaar = 14;
	}
	
	public void zetVolgendeZichtbaar() {
		for(Vorm v:hangmanvormen) {
			if (!v.isZichtbaar()) {
				v.setZichtbaar(true);
				this.aantalonzichtbaar--;
				break;
			}
		}
	}

}
