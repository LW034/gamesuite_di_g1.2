package domain;

import java.awt.Graphics;
import java.util.ArrayList;

public class Tekening implements Drawable{

	private String naam;
	private ArrayList<Vorm> vormen;
	public static final int MIN_X = 0;
	public static final int MAX_X = 399;
	public static final int MIN_Y = 0;
	public static final int MAX_Y = 399;
	
	public Tekening(String naam) throws DomainException {
		if(naam == null || naam.equals("")){
			throw new DomainException("Ongeldige Naam");
		}
		this.naam = naam;
		vormen = new ArrayList<Vorm>();
	}
	
	public String getNaam() {
		return naam;
	}
	
	public void voegToe(Vorm vorm){
		this.vormen.add(vorm);
	}
	public Vorm getVorm(int index){
		return this.vormen.get(index);
	}
	public int getAantalVormen(){
		return vormen.size();
	}
	public void verwijder(Vorm vorm){
		vormen.remove(vorm);
	}
	public boolean bevat(Vorm vorm){
		return this.vormen.contains(vorm);
	}
	@Override
	public String toString(){
		String result = "";
		for (Vorm v : vormen){
			result = result.concat(v.toString()+" ,");
		}
		return result;
	}
	@Override
	public boolean equals(Object o){
	if(o instanceof Tekening){
		Tekening a = (Tekening) o;
	if(this.getAantalVormen() == a.getAantalVormen()){
		for (Vorm z :a.getVormen()){
			if(!vormen.contains(z)){
				return false;
			}
		}
		return true;
		}
	}
		return false;
	}
		
	public ArrayList<Vorm> getVormen(){
		return (ArrayList) vormen.clone();
	}
	public void teken(Graphics graphics){
		for(Vorm v : vormen){
			if(v.isZichtbaar()){
				System.out.println(v.isZichtbaar());
			v.teken(graphics);
			}
		}
	}
}
