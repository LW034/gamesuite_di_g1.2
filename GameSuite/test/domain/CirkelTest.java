package domain;

import static org.junit.Assert.*;
import domain.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CirkelTest {
	
	Cirkel a;
	Cirkel b;
	Cirkel c;

	@Before
	public void setUp() throws Exception {
		 a = new Cirkel(new Punt(100,200),70);
		 b = new Cirkel(new Punt(400,300),50);
		 c = new Cirkel(new Punt(100,200),80);
	}
	
	@Test
	public void equals_geeft_false_als_de_tweede_cirkel_null_is(){
		Cirkel cirkel = null;
		assertFalse(a.equals(cirkel));
	}
	@Test
	public void equals_geeft_false_als_middelpunt_verschillend_is(){
		Cirkel cirkel = new Cirkel(new Punt(100,150),70);
		assertFalse(a.equals(cirkel));
	}
	@Test
	public void equals_geeft_false_als_straal_verschillend_is(){
		Cirkel cirkel = new Cirkel(new Punt(100,200),60);
		assertFalse(a.equals(cirkel));
		Cirkel a = new Cirkel(new Punt(100,200),70);
		Cirkel b = new Cirkel(new Punt(400,300),50);
		Cirkel c = new Cirkel(new Punt(100,200),80);
	}

	@After
	public void tearDown() throws Exception {
	}
	@Test
	public void test_getRadius_geeft_radius_terug(){
		assertEquals(70,a.getRadius());
		}
	@Test
	public void test_getMiddelpunt_geeft_middelpunt_terug(){
		assertEquals(a.getMiddelpunt().getX(),100);
		assertEquals(a.getMiddelpunt().getY(),200);
	}
	@Test
	public void Cirkel_moet_een_Cirkel_maken_met_de_gegeven_coordinaten_en_Middelpunt() {
		assertEquals(a.getMiddelpunt().getX(),100);
		assertEquals(a.getMiddelpunt().getY(),200);
		assertEquals(a.getRadius(), 70);
	}
	@Test(expected = DomainException.class)
	public void Cirkel_moet_exception_gooien_als_middelpunt_null(){
		Cirkel cirkel = new Cirkel(null, 10);
	}
	@Test(expected = DomainException.class)
	public void Cirkel_moet_exception_gooien_als_Straal_kleiner_is_als_0(){
		Cirkel cirkel = new Cirkel(new Punt(100,200),-20);
	}
	@Test(expected = DomainException.class)
	public void Cirkel_moet_exception_gooien_als_straal_gelijk_aan_0_is(){
		Cirkel cirkel = new Cirkel (new Punt(200,100),0);
	}
	@Test
	public void equals_geeft_true_als_middelpunt_en_straal_gelijk_zijn(){
		Cirkel cirkel = new Cirkel(new Punt(100,200),70);
		assertTrue(a.equals(cirkel));
	}
	
}
